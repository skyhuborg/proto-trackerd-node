// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var trackerd_pb = require('./trackerd_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');

function serialize_trackerd_SensorReport(arg) {
  if (!(arg instanceof trackerd_pb.SensorReport)) {
    throw new Error('Expected argument of type trackerd.SensorReport');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_trackerd_SensorReport(buffer_arg) {
  return trackerd_pb.SensorReport.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_trackerd_SensorResponse(arg) {
  if (!(arg instanceof trackerd_pb.SensorResponse)) {
    throw new Error('Expected argument of type trackerd.SensorResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_trackerd_SensorResponse(buffer_arg) {
  return trackerd_pb.SensorResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var TrackerdService = exports.TrackerdService = {
  addSensor: {
    path: '/trackerd.Trackerd/AddSensor',
    requestStream: false,
    responseStream: false,
    requestType: trackerd_pb.SensorReport,
    responseType: trackerd_pb.SensorResponse,
    requestSerialize: serialize_trackerd_SensorReport,
    requestDeserialize: deserialize_trackerd_SensorReport,
    responseSerialize: serialize_trackerd_SensorResponse,
    responseDeserialize: deserialize_trackerd_SensorResponse,
  },
};

exports.TrackerdClient = grpc.makeGenericClientConstructor(TrackerdService);
